#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene= new QGraphicsScene(this);

    count = 0;
    scene->setSceneRect(QRectF(QPointF(0,0), QPointF(0,0)));
    ui->mainView->setScene(scene);
    ui->mainView->setAlignment((Qt::AlignLeft | Qt::AlignTop));
    game = new play;
    globalTimer = new QTimer;
    scene->addWidget(game);
    ui->mainView->resize(550,400);
    ui->mainView->move(0,0);
    ui->playButton->move(60,410);
    ui->retryButton->move(420, 410);
    ui->scoreLabel->move(220,410);
    ui->label->move(260,417);
    ui->comboBox->move(219, 432);
    QString num = QString::number(game->getScore());
    ui->label->setText(num);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
    delete globalTimer;
}

void MainWindow::on_playButton_clicked()
{
    game->run(count);
    connect(globalTimer,SIGNAL(timeout()),this,SLOT(score()));
    globalTimer->start(500);
}

void MainWindow::score(){
    QString num = QString::number(game->getScore());
    ui->label->setText(num);
}

void MainWindow::on_retryButton_clicked()
{
    game->newGame();
}

void MainWindow::keyPressEvent(QKeyEvent *event){
    game->keyPressEvent(event);
}


void MainWindow::on_comboBox_activated(const QString &arg1)
{
   // game->selectRacer(arg1);
    game->setRacer(arg1.toStdString());
    repaint();
}
