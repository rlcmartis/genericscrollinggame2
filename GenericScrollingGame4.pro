#-------------------------------------------------
#
# Project created by QtCreator 2014-06-16T09:18:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GenericScrollingGame4
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    play.cpp

HEADERS  += mainwindow.h \
    play.h

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc
