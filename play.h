#ifndef PLAY_H
#define PLAY_H

#include <QTimer>
#include <QtGui>
#include <QtCore>
#include <QWidget>
#include <QPainter>
#include <string>

using namespace std;

class play : public QWidget
{
    Q_OBJECT
public:
    explicit play(QWidget *parent = 0);
    int x1;
    int x2;
    int x_cone;
    int y_cone;
    int y_car;
    int score;
    string racer;
    void collision();
    void keyPressEvent(QKeyEvent *event);
    void setScore();
    void setRacer(string);
    string getRacer();
    int getScore();
    ~play();

public slots:
    void mySlot();
    void run(int&);
    void newGame();

protected:
    QTimer *myTimer;
    void paintEvent(QPaintEvent *event);



};

#endif // PLAY_H
