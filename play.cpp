#include "play.h"
#include <QDebug>

play::play(QWidget *parent) :
    QWidget(parent)
{
    x1=0;
    x2=600;
    x_cone=200;
    y_cone=250;
    y_car=80;
    myTimer= new QTimer(this);
    score=0;
    racer = ":/images/racer.png";
}

play::~play(){
    delete myTimer;
}

void play::setRacer(string racecar){
    if (racecar == "racer1"){
        racer = ":/images/racer.png";
    }
    else if(racecar == "racer2"){
        racer = ":/images/racer2.png";
    }
    else if(racecar == "racer3"){
        racer = ":/images/racer3.png";
    }
    else if(racecar == "racer4"){
        racer = ":/images/racer4.png";
    }
    else {
        racer = ":/images/racer5.png";
    }
}

string play::getRacer(){
    return racer;
}

void play::paintEvent(QPaintEvent *event){
    QPainter p(this);
    QString racecar = QString::fromStdString(racer);
    QString road = ":/images/coolroad.png";
    QString road2 = ":/images/coolroad.png";
    QString cone = ":/images/cone.png";

    if((y_cone >= y_car-20 && y_cone <= y_car+20) && x_cone == 70){
        collision();
    }

    p.drawPixmap(x1,0,600,400,QPixmap(road));
    p.drawPixmap(x2,0,610,400,QPixmap(road2));


    x1 = x1-10;
    x2 = x2-10;
    x_cone = x_cone-10;

    //cones part
    p.drawPixmap(x_cone,y_cone,50,50,QPixmap(cone));
//80,50
    p.drawPixmap(10,y_car,80,50,QPixmap(racecar));
    if (x1 < -600){
        x1=599;
        y_cone = ((rand() % 12)+1)*20;
        x_cone = 600;
    }

    if (x2 < -600){
        x2=599;
        y_cone = ((rand() % 12)+1)*20;
        x_cone = 600;
    }

    qDebug() << "10" << " "<< x_cone << " " << y_cone << " " << y_car;
    setScore();
}

void play::setScore(){
    score++;
}

int play::getScore(){
    return score;
}

void play::collision(){
    qDebug() << "Collision!!";
    myTimer->stop();
}

void play::mySlot(){

    repaint();
}

void play::run(int &count){
    qDebug()<< "playing";
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    myTimer->start(1);

}

void play::newGame(){
    x1 = 0;
    x2 = 600;
    x_cone = 200;
    y_cone = 250;
    y_car =80;
    score=0;
    myTimer = new QTimer(this);
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    myTimer->start(1);
}

void play::keyPressEvent(QKeyEvent *event){

    qDebug() << y_car;

        if((event->key() == Qt::Key_Up) && y_car > 80){
            y_car = y_car - 20;
        }

        if((event->key() == Qt::Key_Down) && y_car < 280){
            y_car = y_car + 20;
        }

}

//void play


